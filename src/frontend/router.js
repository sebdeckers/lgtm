import { on } from 'bubbly'
import uriTemplates from 'uri-templates'
import { chain } from 'js-deco/dist/commonjs/chain'

const targets = ['target', 'relatedTarget', 'currentTarget', 'srcElement']
function isLink (target) {
  return target && target.tagName === 'A'
}

export default class Router {
  _lastPath = undefined

  constructor (routes = new Map()) {
    this.routes = new Map()
    for (const [pattern, handler] of routes) this.route(pattern, handler)
    window::on('popstate', event => this.trigger(window.location))
    document.addEventListener('click', event => {
      const link = event.path ? event.path.find(isLink)
        : targets.map(target => event[target]).find(isLink)
      if (link && link.origin === window.location.origin) {
        event.preventDefault()
        window.history.pushState(null, '', link.href)
        this.trigger(link)
      }
    })
    setTimeout(() => this.trigger(window.location), 0)
  }

  @chain
  route (pattern, handler) {
    const template = uriTemplates(pattern)
    const predicate = template.fromUri
    this.routes.set(predicate, handler)
  }

  @chain
  trigger (url = '') {
    const path = typeof url === 'string'
      ? url
      : url.pathname + url.search + url.hash
    if (path === this._lastPath) return
    else this._lastPath = path
    for (const [predicate, handler] of this.routes.entries()) {
      const params = predicate(path)
      if (params) {
        handler(params)
        break
      }
    }
  }
}
