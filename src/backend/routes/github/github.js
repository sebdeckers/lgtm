import config from '../../config'
import githubWebhookHandler from 'github-webhook-handler'
import issueComment from './issue_comment'
import pullRequest from './pull_request'
import wildcard from './wildcard'

function log (fn) {
  return (...args) => {
    const retval = this::fn(...args)
    retval.catch(::console.log)
    return retval
  }
}

const handler = githubWebhookHandler({
  path: config.github.webhook.path,
  secret: config.github.webhook.secret
})
handler.on('*', wildcard)
handler.on('issue_comment', log(issueComment))
handler.on('pull_request', log(pullRequest))

export default handler
